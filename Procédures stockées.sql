-- Active: 1649376266639@@127.0.0.1@3306@ecommerce


-- Exercice :
    -- Créer une table product_price_history (id, #product_id, price, date)
DROP TABLE product_price_history;
CREATE TABLE product_price_history (
    id INT PRIMARY KEY AUTO_INCREMENT,
    product_id INT,
    price FLOAT,
    date DATETIME,
    CONSTRAINT FK_pph_product 
        FOREIGN KEY (product_id)
            REFERENCES product(id)
        ON DELETE CASCADE
        -- ON DELETE RESTRICT
        -- ON DELETE SET NULL
        ON UPDATE CASCADE
);

    -- Créer une procédure stockée UPDATE_PRODUCT_PRICE(id, price) qui sauvegarde l'ancien prix dans la table product_price_history puis qui mets à jour le nouveau prix
DROP PROCEDURE IF EXISTS UPDATE_PRODUCT_PRICE;
DELIMITER $$
CREATE PROCEDURE UPDATE_PRODUCT_PRICE(p_id INT, p_price FLOAT)
BEGIN
  DECLARE old_price FLOAT;
  SELECT price 
    INTO @old_price
  FROM product
    WHERE product.id = p_id;
  INSERT INTO product_price_history (product_id, price, `date`) 
    VALUES (p_id, @old_price, NOW());
  UPDATE product 
    SET product.price = p_price
    WHERE product.id = p_id;
END; 
$$
DELIMITER ;

-- Exercice 2 :
    -- Écrire la procédure stockée ANNUAL_REPORT(annee INT) qui écrit dans un fichier CSV le chiffre d'affaires par mois sur l'année
    -- Exemple : CALL ANNUAL_REPORT(2022)
    --      => 2002, 01, 345.87

DELIMITER //
CREATE OR REPLACE PROCEDURE ANNUAL_REPORT(annee INT)

BEGIN 
    SELECT 
        YEAR(o.date) AS ANNEE,
        MONTH(o.date) AS MOIS,
        CONCAT(COALESCE(SUM(ROUND(i.price*quantity,2)), 0), ' €') AS TOTAL_CA 
    INTO OUTFILE '/tmp/rapport.txt'
        FIELDS TERMINATED BY ',' OPTIONALLY ENCLOSED BY '"'
        LINES TERMINATED BY '\n'
    FROM item i 
        INNER JOIN order_table o
            ON o.id = i.order_id
    WHERE 
        YEAR(o.date) = annee
    GROUP BY ANNEE, MOIS;

END;
//
DELIMITER ;