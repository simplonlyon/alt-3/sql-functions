# Mise en pratique


## Normalisation de la base de données
En base de données, une des principales méthodes de conception s'appelle les "[Formes Normales](https://fr.wikipedia.org/wiki/Forme_normale_(bases_de_donn%C3%A9es_relationnelles))", ou FN [autre lien](http://igm.univ-mlv.fr/~dr/XPOSE2011/BDD/fn.html), [et encore](https://stph.scenari-community.org/bdd/0/co/nor1a3.html).

Elles permettent de standardiser le stockage des données en limitant les répétitions de valeurs, ou encore en formalisant les dépendances entre les champs (les colonnes) et la clé.

La base de données `ecommerce` ne respecte pas les 1FN, 2FN, 3FN, on va donc essayer de la normaliser un peu.

### Table product

Dans la table `product`, de nombreuses informations sont répétées (la couleur, le genre).  
Sortez ces informations dans une autre table, et créez les clés étrangères correspondantes.  
Détaillez les étapes qui vous permettent de modifier la structure de votre base pas à pas, sans perdre d'informations, et les différentes requêtes que vous allez exécuter.

#### Détail des étapes : 

  - Créer une table color (ID tinyint unsigned auto_increment primary key, label VARCHAR)
  - Ajouter un champ color_id tinyint unsigned à la table product
  - Mettre à jour le color_id en fonction de la couleur indiquée dans le champ color
  - Supprimer la colonne color après avoir vérifié qu'on avait bien tous les liens

#### Requêtes : 

Création de la table `gender`
```sql
CREATE TABLE IF NOT EXISTS gender 
(
    id INT PRIMARY KEY  AUTO_INCREMENT NOT NULL,
    gender VARCHAR(100)
    
);
```

Création de la table `color`
```sql
CREATE TABLE IF NOT EXISTS color 
(
    id INT PRIMARY KEY  AUTO_INCREMENT NOT NULL,
    color VARCHAR(100)
    
);
```

Insertion des valeurs : 
```sql
INSERT INTO gender (gender) VALUES('Femme'), ('Homme'), ('Enfant');
```
```sql
INSERT INTO color (color) VALUES('noir'), ('blanc'), ('bleu'), ('gris'), ('rouge'), ('vert'), ('orange'), ('jaune');
```

Ajout d'un champ de contrôle :
```sql
ALTER TABLE product ADD IF NOT EXISTS merge_gender BOOLEAN DEFAULT FALSE;
```
```sql
ALTER TABLE product ADD IF NOT EXISTS merge_color BOOLEAN DEFAULT FALSE;
```

Ajout du champ pour la future clé étrangère
```sql
ALTER TABLE product ADD IF NOT EXISTS id_gender INT;
```
```sql
ALTER TABLE product ADD IF NOT EXISTS id_color INT;
```

Mise à jour des clés étrangères dans la table produit : 
```sql
UPDATE product p, color c 
    SET p.id_color = c.id, p.merge_color = TRUE
    WHERE UPPER(p.color) = UPPER(c.color);
```
Autre méthode, avec une sous-requête au lieu d'un produit matriciel
```sql
UPDATE product p 
    SET 
        p.id_gender = (
            SELECT g.id 
                FROM gender g
                WHERE g.gender = p.gender
        ),
        p.merge_gender = TRUE;
```

Suppression des champs contrôle : 
```sql
ALTER TABLE product 
    DROP COLUMN merge_color,
    DROP COLUMN merge_gender;
```

Ajout des clés étrangères : 
```sql
ALTER TABLE product
    ADD CONSTRAINT FK_product_color
        FOREIGN KEY (id_color) REFERENCES color(id),
    ADD CONSTRAINT FK_product_gender
        FOREIGN KEY (id_gender) REFERENCES gender(id);
```


### Table order_table

Sortez dans des tables indépendantes la liste des moyens de paiement (CB, Chèque, Virement, Autre) et des statuts (Panier, En attente de paiement, En attente d'expédition, Expédiée).  
Détaillez les étapes qui vous permettent de modifier la structure de votre base pas à pas, sans perdre d'informations, et les différentes requêtes que vous allez exécuter.

### Archivage des commandes vieilles de plus de 6 mois

Créez une table `order_table_archive` avec la même structure que la table `order_table` sauf l'auto_increment.  

Création de la table en copiant la structure
```sql
CREATE TABLE order_table_archive AS (select * FROM order_table LIMIT 1);
```

Comme le CREATE TABLE ... AS copie aussi les données, on les supprime : 
```sql
DELETE FROM order_table_archive;
```

On remet les contraintes : 
```sql
ALTER TABLE order_table_archive
    -> ADD CONSTRAINT PRIMARY KEY (id),
    -> ADD CONSTRAINT FK_ota_address FOREIGN KEY (address_id) REFERENCES address(id),
    -> ADD CONSTRAINT FK_ota_user FOREIGN KEY (user_id) REFERENCES user(id);
```

Écrivez une procédure stockée ARCHIVE_ORDERS() qui va déplacer les commandes de plus de 6 mois de la table `order_table` à la table `order_table_archive`  

Étapes : 
    - on copie la commande dans la table d'archive des commandes
    - on copie les items dont la commande est présente dans la table d'archive des commandes
    - on supprime les items qui sont présents dans la table d'archive des items
    - on supprime les commandes archivées



```sql

CREATE TABLE item_archive AS (SELECT * FROM item LIMIT 1);
DELETE FROM item_archive;

DELIMITER $$

CREATE OR REPLACE PROCEDURE ARCHIVE_ITEMS()
BEGIN
    INSERT INTO item_archive
    SELECT i.*
        FROM item i
        INNER JOIN order_table_archive ota
            ON ota.id = i.order_id;
    DELETE FROM item
        WHERE id IN (SELECT id FROM item_archive);
END;

CREATE PROCEDURE ARCHIVE_ORDERS()
BEGIN
    INSERT INTO order_table_archive
    SELECT *
        FROM order_table ot
        WHERE ot.date < DATE_SUB(NOW(), INTERVAL 6 MONTH);
    CALL ARCHIVE_ITEMS();
    DELETE FROM order_table 
        WHERE `date` < DATE_SUB(NOW(), INTERVAL 6 MONTH);
END;
$$
DELIMITER ;
```


### Affichage de la liste complète des commandes d'un client

Écrivez la requête qui permet d'afficher la totalité des commandes d'un client, qu'elles soient archivées ou non.

```sql
SELECT * 
    FROM order_table 
    WHERE user_id = 7 
UNION
SELECT * 
    FROM order_table_archive 
    WHERE user_id = 7;
```

### Évaluation du CA mensuel

Écrivez une fonction stockée qui reçoit un float en paramètre, et renvoie un VARCHAR.
  - Si le paramètre est inférieur à 1500€, renvoyer "Déficit"
  - Si le paramètre est compris entre 1500 et 2500, renvoyer "Rentable"
  - Si le paramètre est supérieur à 2500, renvoyer "Exceptionnel".

Modifiez la procédure stockée qui calcule le rapport de l'année, pour rajouter la colonne EVAL au rapport.

### Statistiques

  - Écrivez la requête qui permet de compter le nombre de mois en déficit, le nombre de mois rentables, et le nombre de mois exceptionnels depuis le début des ventes

  - Écrivez la requête qui permet de compter le nombre de mois en déficit, le nombre de mois rentables, et le nombre de mois exceptionnels triés par année