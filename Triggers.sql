-- Active: 1649376266639@@127.0.0.1@3306@ecommerce

-- Exercice : Écrire un trigger qui sauvegarde l'ancien prix dans la table pph avant l'update sur product
-- CREATION DU TRIGGER 
CREATE OR REPLACE TRIGGER before_product_price_update 
BEFORE UPDATE ON product FOR EACH ROW
  INSERT INTO product_price_history (product_id, price, `date`) VALUES (new.id, old.price, NOW());


-- Exercice 2:
    -- rajouter un champ "total" (default 0) dans la table order_table
ALTER TABLE order_table
    ADD COLUMN total FLOAT DEFAULT 0.0;
    -- créer un trigger qui calcule le prix total et le sauvegarde dans le champ lors de l'insertion

        -- Conception : la commande est créée vide pour rattacher les items
            -- => besoin du trigger sur INSERT et UPDATE sur item
            -- => procédure stockée UPDATE_COMMANDE_TOTAL(id, total) => met à jour le total dans la table commande
DELIMITER //
CREATE PROCEDURE UPDATE_COMMANDE_TOTAL(p_id INT, p_total FLOAT)
BEGIN
    UPDATE order_table
        SET total = total + p_total
        WHERE id = p_id;
END;
//
DELIMITER ;
            -- => 2 triggers qui vont appeler la procédure stockée
            --          CALL UPDATE_COMMANDE_TOTAL(old.id, new.qty*new.price)
CREATE TRIGGER update_order_total_after_item_insert AFTER INSERT ON item FOR EACH ROW
    CALL UPDATE_COMMANDE_TOTAL(new.order_id, new.quantity*new.price);
CREATE TRIGGER update_order_total_after_item_update AFTER UPDATE ON item FOR EACH ROW
    CALL UPDATE_COMMANDE_TOTAL(new.order_id, new.quantity*new.price);