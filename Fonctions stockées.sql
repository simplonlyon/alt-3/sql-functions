-- Active: 1649376266639@@127.0.0.1@3306@ecommerce

-- Exercice 1 :  Créez une fonction stockée GET_PRICE_PAID(...) qui permet de calculer le prix payé par un user pour une ligne de commande (un item)
DELIMITER $$
CREATE FUNCTION GET_PRICE_PAID(price FLOAT(5,2), qty INT) RETURNS VARCHAR(20)
BEGIN
    RETURN CONCAT(COALESCE(ROUND(price*qty,2), 0), ' €');
END;
$$
DELIMITER ;

-- Exercice 2 : fonction simple :
-- Écrire une fonction qui reçoit un rôle en paramètre
--      elle le passe en majuscule, et elle renvoie ROLE_<role>
--  Exemple: FORMAT_ROLE('user') => 'ROLE_USER'
DELIMITER $$
CREATE FUNCTION  FORMAT_ROLE(r VARCHAR(255)) RETURNS VARCHAR(260)
BEGIN
    RETURN CONCAT('ROLE_', UPPER(r));
END;
$$
DELIMITER ;

-- Exercice 3 : fonction un peu plus complexe :
--      Écrire une fonction GET_TOTAL_COMMANDE(id_commande) => renvoie le prix total de la commande
-- Indice : utiliser une variable locale pour stocker le résultat

DELIMITER //
CREATE OR REPLACE FUNCTION GET_TOTAL_COMMANDE(id_commande INT) RETURNS FLOAT
BEGIN
    DECLARE total FLOAT;
    SELECT SUM(price*quantity)
        INTO @total
        FROM item
        WHERE order_id = id_commande;
    RETURN ROUND(@total,2);
END;
//
DELIMITER ;

-- Exercice 4 : Gestion des erreurs avec les signaux
-- Modifier le FORMAT_ROLE pour renvoyer une erreur si le rôle reçu est NULL
-- Voir : 
--          - https://mariadb.com/kb/en/signal/ pour la gestion des signaux
--          - https://mariadb.com/kb/en/sqlstate/ pour les codes d'erreur
DELIMITER $$
CREATE OR REPLACE FUNCTION  FORMAT_ROLE(r VARCHAR(255)) RETURNS VARCHAR(260)
BEGIN
    IF (r IS NULL) THEN
        SIGNAL SQLSTATE '23000' SET MYSQL_ERRNO=1048, MESSAGE_TEXT='Merci de fournir un rôle !';
    ELSE
        SIGNAL SQLSTATE '00000' SET MESSAGE_TEXT='Rôle OK, je le formatte';
    END IF;
    RETURN CONCAT('ROLE_', UPPER(r));
END;
$$
DELIMITER ;