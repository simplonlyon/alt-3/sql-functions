-- Active: 1649376266639@@127.0.0.1@3306@ecommerce

-- REQUETES DE STRUCTURE
--    Lister les bdd auxquelles j'ai accès
SHOW DATABASES;
-- En MariaDB/MySQL, le mot-clé SHOW permet de voir les informations DE la base

--    Lister les tables de la bdd courante auxquelles j'ai accès
SHOW TABLES;

--    Lister les champs de la table product avec leur type
DESCRIBE product;

--    Afficher la requête qui a permis de créer la table produit
SHOW CREATE TABLE product

-- REQUETES SELECT (lecture des données stockées dans la base)

-- Sélectionner tous les produits
SELECT * 
    FROM product;

-- Sélectionner tous les produits qui s'adressent aux enfants
SELECT *
    FROM product
    WHERE gender = 'Enfant';

-- Sélectionner tous les produits qui s'adressent aux enfants et qui coûtent plus de 20€
SELECT *
    FROM product
    WHERE gender = 'Enfant'
        AND price > 20;

-- Lister tous les User qui se sont inscrits avec une adresse gmail
SELECT *
    FROM `user`
    WHERE email LIKE '%gmail.com';

-- Afficher toutes les commandes qui ont eu lieu en juin (avec un LIKE)
SELECT *
    FROM order_table
    WHERE `date` LIKE '%-06-%';

-- Afficher toutes les commandes qui ont eu lieu entre le 10 et le 19 juin (avec un LIKE)
    -- AVEC AND, mais pas LIKE snif :'( :'(
SELECT *
    FROM order_table
    WHERE `date` < '2022-06-20'
        AND `date` >= '2022-06-10';
    -- AVEC , mais toujours pas LIKE snif :'(
SELECT *
    FROM order_table
    WHERE `date` BETWEEN '2022-06-10' AND  '2022-06-19';
    -- Avec le LIKE
SELECT *
    FROM order_table
    WHERE `date` LIKE '2022-06-1_';
    -- Le '_' veut dire "n'importe quel caractère mais une et une seule fois"

    -- Afficher pour chaque item le label du produit, la date de la commande, et la qté correspondante
SELECT product.`label`, order_table.`date`, item.`quantity`
    FROM item
    JOIN product
        ON product.id = item.product_id
    JOIN order_table
        ON order_table.id = item.order_id    
        ;

    -- Afficher pour chaque label de produit les villes dans lesquelles il a été livré
SELECT DISTINCT p.label, a.city
    FROM product p
    JOIN item i
        ON i.product_id = p.id
    JOIN order_table o
        ON o.id = i.order_id
    JOIN address a
        ON a.id = o.address_id
    ORDER BY p.label, a.city;
    