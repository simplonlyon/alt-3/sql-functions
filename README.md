# Les fonctions en SQL


## Le but de ce projet est de vous faire découvrir les fonctions en SQL


## Pré-requis
  - Téléchargez le fichier ecommerce.sql joint, et importez-le dans une base de données en local


## Exercice 1 : Afficher le prix payé pour la ligne de commande 2, en arrondissant à 2 chiffres, et en rajoutant le sigle '€' dans le prix
```sql
SELECT CONCAT(ROUND(price*quantity,2), ' €')  AS PRICE_PAID FROM item WHERE id=2;
```
## Exercice 2 : Rajouter le nom du produit dans les données sélectionnées
```sql
SELECT 
    p.label,
    CONCAT(ROUND(i.price*quantity,2), ' €')  AS PRICE_PAID 
    FROM item i 
        INNER JOIN product p 
            ON p.id = i.product_id
    WHERE i.id=2;
```
## Exercice 3 : Afficher tous les prix payés pour chaque produit, trié par nom de produit et prix ascendants
```sql
SELECT 
    p.label,
    CONCAT(ROUND(i.price*quantity,2), ' €') AS PRICE_PAID
    FROM item i
        INNER JOIN product p
            ON p.id = i.product_id 
    ORDER BY
        p.label,
        PRICE_PAID ASC;
```

## Exercice 4 : Afficher les produits qui n'ont jamais été commandés de 2 façons différentes
    - Première version : LEFT JOIN
```sql
SELECT p.* 
    FROM product p 
        LEFT JOIN item i
        -- LEFT JOIN car on a "product JOIN item"
            ON i.product_id = p.id
    WHERE i.product_id IS NULL;
```    
    - Première version bis : RIGHT JOIN
```sql
SELECT p.* 
    FROM item i 
        RIGHT JOIN product p
        -- RIGHT JOIN car on a "item JOIN product"
            ON p.id = i.product_id
    WHERE i.product_id IS NULL;
```

    - Deuxième version : Sous-requête
```sql
SELECT * 
    FROM product
    WHERE id NOT IN (
        SELECT product_id FROM item
    );
```


## Exercice 5 : Ajoutez les produits n'ayant jamais été commandés à la liste de l'exercice 3
```sql
SELECT 
    p.label,
    CONCAT(ROUND(i.price*quantity,2), ' €') AS PRICE_PAID
    FROM item i
        RIGHT JOIN product p
            ON p.id = i.product_id 
    ORDER BY
        p.label,
        PRICE_PAID ASC;
```

## Exercice 6 : Remplacez la valeur "NULL" dans le champ PRICE_PAID par "O €" 
```sql
SELECT 
    p.label,
    CONCAT(COALESCE(ROUND(i.price*quantity,2), 0), ' €') AS PRICE_PAID
    FROM item i
        RIGHT JOIN product p
            ON p.id = i.product_id 
    ORDER BY
        p.label,
        PRICE_PAID ASC;
```

## Exercice 7 : Calculez le chiffre d'affaires généré par chaque produit
```sql
SELECT
    p.label,
    CONCAT(COALESCE(SUM(ROUND(i.price*quantity,2)), 0), ' €') AS TOTAL_CA
    FROM item i
        RIGHT JOIN product p
            ON p.id = i.product_id
    GROUP BY p.label
    ORDER BY
        p.label;
```


## Exercice 8 : calculer le chiffre d'affaires généré au mois de mai 2022
    - Première méthode : fonctions de date
```sql
SELECT 
    YEAR(o.date) AS ANNEE,
    MONTH(o.date) AS MOIS,
    CONCAT(COALESCE(SUM(ROUND(i.price*quantity,2)), 0), ' €') AS TOTAL_CA 
    FROM item i 
        INNER JOIN order_table o
            ON o.id = i.order_id
    WHERE 
        YEAR(o.date) = 2022
        AND MONTH(o.date) = 5
    GROUP BY ANNEE, MOIS;
```
    - Deuxième méthode : Between
```sql
SELECT 
    YEAR(o.date) AS ANNEE,
    MONTH(o.date) AS MOIS,
    CONCAT(COALESCE(SUM(ROUND(i.price*quantity,2)), 0), ' €') AS TOTAL_CA 
    FROM item i 
        INNER JOIN order_table o
            ON o.id = i.order_id
    WHERE 
        o.date BETWEEN '2022-05-01' AND '2022-05-31'
    GROUP BY ANNEE, MOIS;
```

    - Troisième méthode : Triche !
```sql
SELECT 
    YEAR(o.date) AS ANNEE,
    MONTH(o.date) AS MOIS,
    CONCAT(COALESCE(SUM(ROUND(i.price*quantity,2)), 0), ' €') AS TOTAL_CA 
    FROM item i 
        INNER JOIN order_table o
            ON o.id = i.order_id
    WHERE 
        -- MySQL/MariaDB permettent de traiter les dates comme des VARCHAR
        o.date LIKE '2022-05%'
    GROUP BY ANNEE, MOIS;
```
## Exercice 9 : Afficher les mois pour lesquels le chiffre d'affaire a dépassé 250€
```sql
SELECT 
    YEAR(o.date) AS ANNEE,
    MONTH(o.date) AS MOIS,
    COALESCE(SUM(ROUND(i.price*quantity,2)), 0) AS TOTAL_CA 
    FROM item i 
        INNER JOIN order_table o
            ON o.id = i.order_id
    GROUP BY ANNEE, MOIS
    HAVING TOTAL_CA > 250;
```

## Exercice 9 : Afficher les mois pour lesquels le chiffre d'affaire a dépassé 250€ Uniquement en 2022
```sql
SELECT 
    YEAR(o.date) AS ANNEE,
    MONTH(o.date) AS MOIS,
    COALESCE(SUM(ROUND(i.price*quantity,2)), 0) AS TOTAL_CA 
    FROM item i 
        INNER JOIN order_table o
            ON o.id = i.order_id
    WHERE YEAR(o.date) = 2022
    GROUP BY ANNEE, MOIS
    HAVING TOTAL_CA > 250;
```